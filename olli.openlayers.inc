<?php

/**
 * @file
 * Provides hooks for integration with OpenLayers (http://drupal.org/project/openlayers)
 */

/**
 * Implements hook_openlayers_maps().
 */
function olli_openlayers_maps() {
  $map = new stdClass();
  $map->api_version = 1;
  $map->name = 'olli_formatter_map';
  $map->title = t('OpenLayers Image Map');
  $map->description = t('A Map Used for displaying images');
  $map->data = array(
    'width' => '100%',
    'height' => '400px',
    'image_path' => drupal_get_path('module', 'openlayers') . '/themes/default_dark/img/',
    'css_path' => drupal_get_path('module', 'openlayers') . '/themes/default_dark/style.css',
    'proxy_host' => '',
    'hide_empty_map' => 0,
    'center' => array(
      'initial' => array(
        'centerpoint' => '0, 0',
        'zoom' => '1',
      ),
      'restrict' => array(
        'restrictextent' => 0,
        'restrictedExtent' => '',
      ),
    ),
    'default_layer' => 'olli',
    'layers' => array(
      'olli' => 'olli',
    ),
    'behaviors' => array(
      'openlayers_behavior_keyboarddefaults' => array(),
      'openlayers_behavior_navigation' => array(
        'zoomWheelEnabled' => 0,
        'zoomBoxEnabled' => 1,
        'documentDrag' => 0,
      ),
      'openlayers_behavior_panzoombar' => array(
        'zoomWorldIcon' => 0,
        'panIcons' => 1,
      ),
    ),
    'projection' => 'EPSG:900913',
    'displayProjection' => 'EPSG:4326',
    'styles' => array(
      'default' => 'default',
      'select' => 'default',
      'temporary' => 'default',
    ),
    'map_name' => 'olli_formatter_map',
  );

  return array(
    'olli_formatter_map' => $map,
  );
}


/**
 * Formatter layers
 */
function olli_openlayers_layers() {
  $layers = array();
  $layer = new stdClass();
  $layer->api_version = 1;
  $layer->name = 'olli';
  $layer->title = 'Placeholder for olli Formatter';
  $layer->description = '';
  $layer->data = array(
    'layer_type' => 'openlayers_layer_type_image',
    'projection' => array('EPSG:900913'),
  );
  $layers[$layer->name] = $layer;
  return $layers;
}
