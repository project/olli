/**
 * OpenLayers Image Layer Handler
 */
Drupal.openlayers.layer.image = function(title, map, options) {
  options.options = options.options || {};

  // Note, so that we do not pass all the features along to the Layer
  // options, we use the options.options to give to Layer
  options.options.drupalID = options.drupalID;

  options.options.styleMap = Drupal.openlayers.getStyleMap(map, options.drupalID);

  var layer = new OpenLayers.Layer.Image(
    options.name,
    options.file.uri,
    new OpenLayers.Bounds(-180, -90, 180, 90),
    new OpenLayers.Size(options.file.width/options.factors.x, options.file.height/options.factors.y),
    {numZoomLevels: options.numZoomLevels}
  );

  if (options.features) {
    Drupal.openlayers.addFeatures(map, layer, options.features);
  }

  return layer;
};
