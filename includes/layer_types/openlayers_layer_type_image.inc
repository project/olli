<?php
/**
 * @file
 * Image Layer Type
 * http://dev.openlayers.org/docs/files/OpenLayers/Layer/Image-js.html
 */

/**
 * Define the Ctools plugin options.
 */
function olli_openlayers_layer_type_image_openlayers_layer_types() {
  return array(
    'title' => t('Image'),
    'description' => t('Use an image as a layer.'),
    'class' => 'openlayers_layer_type_image',
    'layer_type' => array(
      'file' => 'openlayers_layer_type_image.inc',
      'class' => 'openlayers_layer_type_image',
      'parent' => 'openlayers_layer_type',
    ),
  );
}

/**
 * OpenLayers Image Layer Type class
 */
class openlayers_layer_type_image extends openlayers_layer_type {

  /**
   * Provide initial values for options.
   */
  function options_init() {
    return parent::options_init() +
      array(
      'layer_handler' => 'image',
      'file' => NULL,
    );
  }

  /**
   * Options form which generates layers
   */
  function options_form($defaults = array()) {
    return array(
      'name' => array(
        '#type' => 'textfield',
        '#title' => 'Layer title',
        '#default_value' => isset($this->data['name']) ? $this->data['name'] : '',
      ),
      'file' => array(
        '#name' => 'files[imagelayer]',
        '#type' => 'managed_file',
        '#title' => t('Image'),
        '#default_value' => isset($this->data['file']) ? $this->data['file'] : '',
        '#upload_location' =>  'public://',
      ),
      'factors' => array(
        '#tree' => TRUE,
        'x' => array(
           '#title' => 'Width divider',
           '#type' => 'textfield',
           '#default_value' => isset($this->data['factors']['x']) ? $this->data['factors']['x'] : 1
        ),
        'y' => array(
           '#title' => 'Height divider',
           '#type' => 'textfield',
           '#default_value' => isset($this->data['factors']['y']) ? $this->data['factors']['y'] : 1
        ),
      ),
      'numZoomLevels' => array(
         '#title' => 'Zoomlevels',
         '#type' => 'textfield',
         '#default_value' => isset($this->data['numZoomLevels']) ? $this->data['numZoomLevels'] : 1
      ),
    );
  }

 /**
  * hook_validate() of the form.
  */
  function options_form_validate($form, &$form_state) {
    $form_state['data']['numZoomLevels'] = intVal($form_state['data']['numZoomLevels']);
    $form_state['data']['factors']['x'] = intVal($form_state['data']['factors']['x']);
    $form_state['data']['factors']['y'] = intVal($form_state['data']['factors']['y']);

    if ($file = file_load($form_state['data']['file'])) {
      // Do something with the file if needed.
    } else {
      form_set_error($form_state['data']['layer_type'].'][file', 'Cannot access the file.');
    }
  }

  /**
   * hook_submit() of the form.
   */
  function options_form_submit($form, &$form_state) {
    global $user;

    $item = $form_state['item'];
    if (isset($item->data['file']) && $file = file_load($item->data['file'])) {
      file_delete($file);
    }

    if (isset($form_state['values']['data']['file']) && $file = file_load($form_state['values']['data']['file'])) {
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      file_usage_add($file, 'olli', 'openlayers_layer_type', $user->uid);
    }
  }

  /*
   * What to do when we delete the layer: delete the file.
   */
  function delete($item) {
    $file = file_load($item->data['file']);
    file_delete($file);
   }

  /**
   * Render.
   */
  function render(&$map) {
    if (isset($this->data['file'])) {
      if ($file = file_load($this->data['file'])) {
        $infos = image_get_info($file->uri);
        $this->data['file'] = array();
        $this->data['file']['uri'] = file_create_url($file->uri);
        $this->data['file']['width'] = $infos['width'];
        $this->data['file']['height'] = $infos['height'];
      }
    }
    drupal_add_js(drupal_get_path('module', 'olli') . '/includes/layer_types/openlayers_layer_type_image.js');
  }
}
