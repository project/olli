<?php

/**
 * @file
 * Drupal field formatter hooks and helper functions.
 */

/**
 * Implements hook_field_formatter_info().
 */
function olli_field_formatter_info() {
  $formatters = array(
    'olli_file' => array(
      'label' => t('OpenLayers Image map'),
      'field types' => array('file', 'image'),
      'settings' => array(
        'data' => 'full',
        'map_preset' => 'olli_formatter_map',
      ),
    ),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function olli_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  // Map preset formatter
  if ($display['type'] == 'olli_file' && module_exists('openlayers')) {
    // Get preset options, filtered to those which have the GeoField placeholder layer
    $presets = openlayers_presets();
    $preset_options = array();

    foreach ($presets as $preset) {
      if (in_array('olli', $preset->data['layers'])) {
        $preset_options[$preset->name] = $preset->title;
      }
    }

    $element['map_preset'] = array(
      '#title' => t('OpenLayers Preset'),
      '#type' => 'select',
      '#default_value' => $settings['map_preset'] ? $settings['map_preset'] : 'olli_formatter_map',
      '#required' => TRUE,
      '#options' => $preset_options,
      '#description' => t('Select which OpenLayers map you would like to use. Only maps which have the geofile placeholder layer may be selected. If your preferred map is not here, add the geofile placeholder layer to it first.'),
    );
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function olli_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();

  if ($display['type'] == 'olli_file') {
    if (!empty($settings['map_preset'])) {
      $openlayers_presets = openlayers_preset_options();
      $summary[] = t('Openlayers Map: @data', array('@data' => $openlayers_presets[$settings['map_preset']]));
    }
  }

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function olli_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'olli_file':
      $map_name = $display['settings']['map_preset'] ? $display['settings']['map_preset'] : 'geofile_formatter_map';
      $output = _olli_openlayers_formatter($map_name, $items, $display['settings']);
      $element[0] = array('#markup' => $output);
      return $element;
  }

  return $element;
}

function _olli_openlayers_formatter($map_name, $items, $options) {
  $preset = openlayers_preset_load($map_name);
  $map    = openlayers_build_map($preset->data);

  foreach ($items as $key => $delta) {
    $layer = openlayers_layer_type_load('openlayers_layer_type_image');
    $layer->data['file'] = $delta['fid'];
    $map['layers']['olli'] = $layer->render($map);
   }

  // Return themed map if no errors found
  if (empty($map['errors'])) {
    $js = array('openlayers' => array('maps' => array($map['id'] => $map)));
    drupal_add_js($js, 'setting');

    // Push map through theme function and return
    $output = theme('openlayers_map', array(
      'map' => $map,
      'map_name' => $map_name
    ));
  }
  return $output;
}
